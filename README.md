# MLRegressionCalo

Example of ML regression - prediction of particle direction from the calorimeter image

# Prerequisits
 * Python3 with the following libraries:
   * tensorflow2
   * numpy
   * JSON

It is recommended to install everything through [conda](https://conda.io/projects/conda/en/latest/user-guide/getting-started.html)
  

# Instructions
 *  Get the code
```
git clone https://gitlab.cern.ch/andrii/mlregressioncalo
```
 * Extract the data
```
cd mlregressioncalo/data
tar -xzf data.tgz
```
 * Run the training
```
cd mlregressioncalo
python train.py data/tmp_proton_00*
```

If successfull, you should see something like that in the output log (after the standard tensorflow messages):
```
...
Epoch 1/100
3993/3993 - 79s - 20ms/step - loss: 133.6543 - mean_absolute_error: 133.6543 - mean_squared_error: 413669.0938 - val_loss: 45.4585 - val_mean_absolute_error: 45.4585 - val_mean_squared_error: 3810.7722 - learning_rate: 0.0010
Epoch 2/100
3993/3993 - 78s - 19ms/step - loss: 36.5914 - mean_absolute_error: 36.5914 - mean_squared_error: 3420.7397 - val_loss: 25.0568 - val_mean_absolute_error: 25.0568 - val_mean_squared_error: 1277.3058 - learning_rate: 0.0010
Epoch 3/100
3993/3993 - 78s - 20ms/step - loss: 23.6379 - mean_absolute_error: 23.6379 - mean_squared_error: 1269.1769 - val_loss: 20.6239 - val_mean_absolute_error: 20.6239 - val_mean_squared_error: 844.7758 - learning_rate: 0.0010
Epoch 4/100
3993/3993 - 79s - 20ms/step - loss: 18.5255 - mean_absolute_error: 18.5255 - mean_squared_error: 866.4612 - val_loss: 18.2696 - val_mean_absolute_error: 18.2696 - val_mean_squared_error: 687.0389 - learning_rate: 0.0010
Epoch 5/100
...
```
 * Visualize the results
```
python visualize.py history.json
```


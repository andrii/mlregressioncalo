import tensorflow as tf
from tensorflow import keras
import sys
import numpy as np
import json

# fixing GPU RTX 2080 specific problem on Andrii's desktop
physical_devices = tf.config.list_physical_devices('GPU')           # GPU RTX 2080 issue
tf.config.experimental.set_memory_growth(physical_devices[0], True) # GPU RTX 2080 issue 

def load_numpy_data_multifile(filelist):
    allindata  = []
    for fname in filelist:
        try:
            item = np.load(
                    fname,
                    allow_pickle=True,
                    encoding="bytes"
            )['arr_0']  # all data
            if not item.shape[0]: print ("[load_numpy_data_multifile] empy file, skipping: ",fname)
            else: allindata.append(item)
        except Exception as e:
            print ("[load_numpy_data_multifile] failed to load file: ",fname)
            print ("[load_numpy_data_multifile]  ... the error: ")
            print (e)
            print ("[load_numpy_data_multifile]  ... skipping this file.")
    data = np.concatenate(allindata) if allindata else np.array(allindata)
    return data

def prepare_data(data,verbose=False,trkdtype='uint8',normcharge=False):
    #
    # The function prepares the data in the proper-shape numpy arrays
    #  
    #   data - normally, it is a numpy array of the data for training or prediction
    #
    # Data structure:
    #   [:,0] - calo images
    #   [:,1] - calo 2 variables  - bgoene (CALO total energy), maxbar (CALO energy of maximum bar)
    #   [:,2] - truth 4 variables - normally variables that are targeted at the regression optimisation, say x_bot,x_top,y_bot,y_top
    #   [:,3] - rec   4 variables - the same as above, but obtained from the standard BGO rec direction, instead of the truth direction


    caloimages    = data[:,0]
    calodata      = data[:,1]
    truthdata     = data[:,2]
    recdata       = data[:,3]

    # get tensor-like shape of the arrays
    caloimages    = caloimages.tolist()
    calodata      = calodata.tolist()
    truthdata     = truthdata.tolist()
    recdata       = recdata.tolist()

    caloimages    = np.array(caloimages,dtype='float16') # get a 3-dimansional array, 1st dimension - events, 2,3rd dimensions - image dimensions
    calodata      = np.array(calodata)                   #  
    truthdata     = np.array(truthdata)                  #  
    recdata       = np.array(recdata)                    #

    # normalize calo images
    maxpixsignl = np.max(caloimages,axis=(1,2))                 # maximim pixel signal per image
    maxpixsignl = maxpixsignl.reshape((len(maxpixsignl),1,1,1)) #
    caloimages  = np.divide(caloimages,maxpixsignl)             #
    
    # all done
    return {
        'caloimages'    : caloimages, 
        'calodata'      : calodata,
        'truthdata'     : truthdata, 
        'recdata'       : recdata,
    }

def get_input_data():
    # get all input data
    args = sys.argv[1:]
    np.random.seed(1234)
    np.random.shuffle(args)
    data = load_numpy_data_multifile(args)
    # randomly shuffle the sample
    np.random.shuffle(data)
    
    # split data into training and validation
    val_ratio  = 0.1
    val_ratio  = int(data.shape[0] * val_ratio)
    data_train = data[:-val_ratio]
    data_val   = data[-val_ratio:]
    
    # input data size info
    print ("data.shape =       ",data.shape)
    print ("data_train.shape = ",data_train.shape)
    print ("data_val.shape   = ",data_val.shape)
    
    # 'prepare' input data (in particular, normalize BGO image, etc.)
    dt_      = prepare_data(data_train)
    dv_      = prepare_data(data_val)
    dt_truth = dt_['truthdata'] 
    dv_truth = dv_['truthdata'] 
    dt_in    = [dt_['caloimages'],    dt_['calodata']] 
    dv_in    = [dv_['caloimages'],    dv_['calodata']] 
    
    return dt_in, dt_truth, dv_in, dv_truth

def get_convnet_calo_model(out_shape=4,dropout=None):
    # inputs
    inputCalo    = keras.Input(shape=(14 , 22,   1))
    inputEnergy  = keras.Input(shape=(2,))

    # Calo model image
    caloconlayer    = keras.layers.Conv2D(128,(4,4), activation="relu")(inputCalo)
    caloconlayer    = keras.layers.Dropout(dropout)(caloconlayer) if dropout else caloconlayer
    caloconlayer    = keras.layers.Conv2D(64,(4,4), activation="relu")(caloconlayer)
    caloconlayer    = keras.layers.Dropout(dropout)(caloconlayer) if dropout else caloconlayer
    caloconlayer    = keras.layers.Conv2D(32,(4,4), activation="relu")(caloconlayer)
    caloconlayer    = keras.layers.Dropout(dropout)(caloconlayer) if dropout else caloconlayer
    caloconlayermid = keras.Model(inputs=inputCalo,outputs=caloconlayer)
    # ... full-size filters of conv net
    caloconlayer    = keras.layers.Conv2D(100,(caloconlayermid.output_shape[1],caloconlayermid.output_shape[2]), activation="relu")(caloconlayer)
    caloconlayer    = keras.layers.Dropout(dropout)(caloconlayer) if dropout else caloconlayer
    caloconlayer    = keras.layers.Flatten()(caloconlayer)
    caloconlayer    = keras.Model(inputs=inputCalo,outputs=caloconlayer)

    # Calo model energy (simple linear)
    caloenergy      = keras.layers.Dense(2,kernel_initializer='random_uniform',activation="linear")(inputEnergy)
    caloenergy      = keras.Model(inputs=inputEnergy,outputs=caloenergy)

    # Calo model cobined (multi-layer perceptron)
    calo = keras.layers.concatenate([caloconlayer.output, caloenergy.output])
    calo = keras.layers.Dense(50, activation="relu")(calo)
    calo = keras.layers.Dropout(dropout)(calo) if dropout else calo
    calo = keras.layers.Dense(out_shape, activation="linear")(calo)
    calo = keras.layers.Dense(out_shape, activation=None,use_bias=True)(calo) 
    calo = keras.Model(inputs=[caloconlayer.input,caloenergy.input],outputs=calo)

    # Compile the model
    calo.compile(loss='mean_absolute_error', optimizer='adam', metrics=['mean_absolute_error','mean_squared_error'])
    #calo.compile(loss='mean_squared_error', optimizer='adam', metrics=['mean_squared_error','mean_absolute_error'])

    # model output shape 
    print ("Calo convnet model prepared, outputdimension:", calo.output_shape)

    # return the model
    return calo


def run():
    # get data
    dt_in, dt_truth, dv_in,dv_truth = get_input_data()

    # get the ML model
    model=get_convnet_calo_model()

    # adjust learning rate
    reduce_lr = keras.callbacks.ReduceLROnPlateau(
        monitor='val_loss', factor=0.5, patience=4, verbose=1, mode='auto',
        min_delta=0.0001, cooldown=0, min_lr=0.0000001,
    )

    # fit
    history = model.fit(dt_in, dt_truth, validation_data=(dv_in,dv_truth),epochs=100, verbose=2, callbacks = [reduce_lr]) # step 1
    #history = model.fit(dt_in, dt_truth, validation_data=(dv_in,dv_truth),epochs=100, verbose=2) # step 1

    # save trainig history
    with open('history.json', 'w') as fh:
        del history.history['lr'] # for some reason it can not be serialised by json
        json.dump(history.history, fh)
        
    # save the model
    model.save('model.h5')
    
if __name__ == "__main__":
    run()
   
